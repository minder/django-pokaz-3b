#coding: utf-8
from django.conf.urls import patterns, include, url
from django.views.generic import ListView, DetailView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# Uncomment the next two lines to enable the admin:
from grzejnik.models import Fotka
from django.views.generic import ListView, DetailView
from django.contrib import admin
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^kaloryfery/', include('grzejnik.urls')),

    url(r'^$','grzejnik.views.hello'),
    (r'^pokaz_form/$', 'grzejnik.views.pokaz_form'),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^foto/$', ListView.as_view(model=Fotka)),
)

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )
   
