#coding: utf-8
#plik: grzejnik/models.py
from django.db import models
from django.core.urlresolvers import reverse

# SOUTH (django-south) - umożliwia migracje bazy danych,
# tzn. dokonywanie zmian w modelach i odzwierciedlanie
# ich w bazie (aktualizację)

class Kaloryfer(models.Model):
    marka = models.CharField(max_length=50)
    model = models.CharField(max_length=50)
    szer = models.FloatField()
    wys = models.FloatField()
    gleb = models.FloatField()
    moc = models.IntegerField()
    kolor = models.CharField(max_length=16, blank=True)
    zebra = models.IntegerField()
    data_prod = models.DateField()
    dl_gwar = models.IntegerField()
    opis = models.TextField(blank=True)

    def __unicode__(self):
        return "%s %s %s" % (self.marka, self.model, self.moc)

    def get_url(self):
        return reverse('grzejnik_detal', kwargs={'pk':self.id})


class Producent(models.Model):
    pass

class Fotka(models.Model):
    plik = models.ImageField(upload_to="foto")
    opis = models.TextField()
    
    def __unicode__(self):
        return self.opis[:20]
