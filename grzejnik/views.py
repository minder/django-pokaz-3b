#coding: utf-8
# views.py
from django.shortcuts import render_to_response
from grzejnik.forms import KaloryferForm, PokazForm
from grzejnik.models import Kaloryfer
from django.core.context_processors import csrf
from django.http import HttpResponse

def hello(request):
    return render_to_response('hello.html',{})

def kaloryfer_edit(request, pk):
    if request.method == 'POST':
        # formularz został wypełniony i przesłany
        formularz = KaloryferForm(request.POST)
        if formularz.is_valid():
            # można zapisać itd.
            return HttpResponse('OK')
            # w normalnym przypadku powinno być przekierowanie
        else:
            dane = {
                'formularz': formularz,
            }
            dane.update(csrf(request))
            return render_to_response('kaloryfer_edit.html',
            dane)
        
    else:
        # strona jest otwierana "na świeżo"
        obj = Kaloryfer.objects.get(pk=pk)
        formularz = KaloryferForm(instance=obj)
        dane = {
            'formularz': formularz,
        }
        dane.update(csrf(request))
        
        return render_to_response('kaloryfer_edit.html',
            dane)
    
def pokaz_form(request):
    dane = {}
    if request.method == "POST":
        # analiza przesłanych danych
        formularz = PokazForm(request.POST)
        if formularz.is_valid():
            napis = u"Brawo! Udało się przesłać poprawnie"
        else:
            napis = u"Niestety, wystąpił błąd"
    else:
        # tworzenie pustego formularza
        formularz = PokazForm()
        napis = "Tworzymy od nowa"
    
    dane.update(csrf(request))
    dane['napis'] = napis
    dane['formularz'] = formularz
    return render_to_response('pokaz_form.html', dane)
    
    
