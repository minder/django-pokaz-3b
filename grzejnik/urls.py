from django.conf.urls import patterns, include, url
from grzejnik.models import Kaloryfer
from django.views.generic import ListView, DetailView


urlpatterns = patterns('',

    url(r'^$',
        ListView.as_view(model=Kaloryfer),
        name='grzejnik_lista'
        ),
    url(r'^(?P<pk>\d+)/$',
        DetailView.as_view(model=Kaloryfer),
        name="grzejnik_detal"
        ),
    url(r'^(?P<pk>\d+)/edit/$',
        'grzejnik.views.kaloryfer_edit',
        name="grzejnik_edycja"),
)
